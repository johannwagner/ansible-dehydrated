---

- name: include os specific vars
  include_vars: "{{ ansible_distribution|lower|replace(' ', '_') }}.yml"

- name: install dehydrated
  become: true
  package:
    name:
      - dehydrated
      - curl
      - jq
  notify: register account

- name: create acmedns register script
  become: true
  template:
    src: create_acmedns_account.sh
    dest: "/etc/dehydrated/create_acmedns_account.sh"
    mode: 0740
  when: dehydrated__dns_challenge and dehydrated__acmedns_credentials is undefined

- name: deploy config files
  become: true
  template:
    src: '{{ item }}'
    dest: '/etc/dehydrated/{{ item }}'
    owner: '{{ dehydrated__user }}'
    group: '{{ dehydrated__group }}'
    mode: 0644
  loop:
    - config
    - domains.txt
  notify: start dehydrated

- name: deploy dehydrated vhost
  become: true
  template:
    src: vhost
    dest: /etc/nginx/vhosts.d/dehydrated.conf  # FIXME: rename to default.conf
    mode: 0644
  notify: reload nginx
  when: dehydrated__nginx

- name: create data directories
  become: true
  file:
    name: '{{ item }}'
    state: directory
    owner: '{{ dehydrated__user }}'
    group: '{{ dehydrated__group }}'
    mode: 0755
  loop:
    - '{{ dehydrated__challengesdir }}'
    - /var/lib/dehydrated
    - '{{ dehydrated__certdir | default("/var/lib/dehydrated/certs") }}'

- name: create hook handler
  become: true
  template:
    src: hook.sh
    dest: /etc/dehydrated/hook.sh
    group: '{{ dehydrated__group }}'
    mode: 0750

- name: create hook directory
  become: true
  file:
    name: /etc/dehydrated/hooks.d
    state: directory
    owner: root
    group: '{{ dehydrated__group }}'
    mode: 0750

- name: create nginx hook
  become: true
  template:
    src: deploy-cert-nginx
    dest: /etc/dehydrated/hooks.d/deploy-cert-nginx
    group: '{{ dehydrated__group }}'
    mode: 0750
  when: dehydrated__nginx

- name: create acme-dns hook
  become: true
  copy:
    src: files/acmedns-hook.sh
    dest: /etc/dehydrated/hooks.d/acmedns-hook.sh
    group: '{{ dehydrated__group }}'
    mode: 0750
  when: dehydrated__dns_challenge

- name: allow dehydrated to run hooks as root
  become: true
  template:
    src: sudoers-dehydrated
    dest: /etc/sudoers.d/dehydrated
    mode: 0440
  when: ansible_distribution | lower == 'opensuse leap'

# FIXME: verify all legacy systems are migrated and remove following two tasks
- name: remove old sudoers file
  become: true
  file:
    path: /etc/sudoers.d/dehydrated-nginx
    state: absent

- name: move certs from previous location
  become: true
  command:
    cmd: "mv /etc/ssl/private/{{ dehydrated__domain[0].name }} /var/lib/dehydrated/certs"
    removes: "/etc/ssl/private/{{ dehydrated__domain[0].name }}"
  when: dehydrated__certdir is not defined
  notify: run dehydrated deploy hook

- name: template systemd unit
  become: true
  template:
    src: '{{ item }}'
    dest: /usr/lib/systemd/system/{{ item }}
    mode: 0644
  loop:
    - dehydrated.service
    - dehydrated.timer
  when: ansible_distribution | lower == 'ubuntu'
  notify: [ daemon-reload ]

- name: check if acmedns account is registered
  become: true
  stat:
    path: /etc/dehydrated/acmedns-creds.json
  register: dehydrated__acmedns_account_exists
  when: dehydrated__dns_challenge

- name: setup ACME DNS Account 
  include_tasks: acmedns_setup.yml
  when: dehydrated__dns_challenge and not dehydrated__acmedns_account_exists.stat.exists

- name: enable dehydrated
  become: true
  systemd:
    name: dehydrated.timer
    enabled: true
    state: started
